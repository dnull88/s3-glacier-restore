#!/usr/bin/env python

#Created by dnull /dnull@zeoalliance.com/
#v.1.1.2


import boto3
import sys,re
import argparse

parser = argparse.ArgumentParser(
    prog="glacierFile",
    usage="%(prog)s [options] mode s3://path/to/",
    description="Tool for restoring glacier files", add_help=False)

parser.add_argument("--help", action='help',
                    help="show this help message and exit")

parser.add_argument("--only-glacier", dest="glacieronly", action="store_true",
                    help="show only glacier objects, recursively in all sub directories")
parser.add_argument("--full-path", dest="restorepath", action="store_true",
                    help="restore all glacier objects in path recursively (including all subdirectories)")

parser.add_argument("mode", help="list or restore mode",choices=['list','restore'])
parser.add_argument("path", help="path to s3 file or directory")

args = parser.parse_args()



backet_name = ""

def tlistObjects(path='/'):
    if not path.endswith("/") and len(path)>0:
        path=path + "/"

    s3 = boto3.resource('s3')
    bucket = s3.Bucket(backet_name)
    objects = []
    print("|StorageType|\t|FileKey|\t|Status|")
    print("----------------------------------------")

    result = bucket.meta.client.list_objects(Bucket=bucket.name,
                                             Delimiter='/', Prefix=path)
    if "CommonPrefixes" in result:
        for subdir in result.get("CommonPrefixes"):
            print("SUBDIR\t\t s3://{}/{}\t".format(backet_name,subdir.get("Prefix")))

    if "Contents" in result:
        for k in result.get("Contents"):
            if k.get("Size") != 0:
                storage_class = k.get("StorageClass")
                key_name = k.get("Key")
                if storage_class == "GLACIER":
                    restore_str = s3.Object(bucket.name, key_name).restore
                    try:
                        restore_str = restore_str.replace('ongoing-request="false"', 'Restored').replace(
                            'ongoing-request="true"', 'Restoring...')
                    except:
                        pass
                    print("GLACIER\t\t s3://{}/{}\t {}".format(backet_name, key_name, restore_str))
                else:
                    print("{}\t s3://{}/{}".format(storage_class, backet_name, key_name))




def listGlacierOnlyRecursive(path=''):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(backet_name)

    for obj in bucket.objects.filter(Prefix=path):
        if obj.storage_class == 'GLACIER':
            restore_str = s3.Object(bucket.name, obj.key).restore
            try:
                restore_str = restore_str.replace('ongoing-request="false"', 'Restored').replace(
                    'ongoing-request="true"', 'Restoring...')
            except:
                pass
            print("{}\t\t s3://{}/{}\t {}".format(obj.storage_class, backet_name, obj.key,restore_str))



def restorePathRecursive(path=''):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(backet_name)

    for obj in bucket.objects.filter(Prefix=path):
        if obj.storage_class == 'GLACIER':
            restoreObject(obj.key)


def restoreObject(filekey,days=5):
    s3 = boto3.client('s3')
    s3res = boto3.resource('s3')

    obj = s3res.Object(backet_name, filekey)

    if obj.storage_class == 'GLACIER':
        try:

            s3.restore_object(Bucket=backet_name, Key=filekey,
                          RestoreRequest={'Days': days})
            print("OK. Restoring {} started.".format(filekey))
        except:
            print("Error when restoring {}. Maybe restoration already in progress.".format(filekey))
    else:
        print("Object {} is not a glacier.".format(filekey))

def main():

    pattern = re.compile("s3://.*?\S+")

    if pattern.match(args.path):
        full_path = args.path
        split_path = full_path.split("/",3)
        global backet_name
        backet_name = split_path[2]

        if args.mode == "list":
            if args.glacieronly:
                listGlacierOnlyRecursive(split_path[3])
            else:
                try:
                    tlistObjects(split_path[3])
                except IndexError:
                    pass

        elif args.mode == "restore":
            if args.restorepath:
                restorePathRecursive(split_path[3])
            else:
                restoreObject(split_path[3])
    else:
        print("Bucket path does not match regexp. Use s3://bucket/path/to")



if __name__ == '__main__':
    main()
